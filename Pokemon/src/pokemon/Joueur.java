package pokemon;

import java.util.ArrayList;

public class Joueur {
	
	private ArrayList<Objet> inventaire;

	public boolean capturer(Attrapeball attrapeball, int vieAdversaire, int vieMaxAdversaire) {
		
		double d = Math.random();
		float taux_capture = attrapeball.proba_capture / 2*(vieAdversaire / vieMaxAdversaire);  // comme ca a la moitié de sa vie on a la probabilité de la pokeball de l'attraper
		
		if (d < taux_capture) {
			capture = true;
		}
	}
	
	
	public boolean fuir(niveauPokemon1, niveauPokemon2) {
		
		double d = Math.random();
		float seuil = niveauPokemon1/(Math.pow(niveauPokemon2, 1,3));
		
		if ( d < seuil) {
			fuite = true;	
		}
	}
	
	public void interagir() {
		//Active la méthode du NPC (soigne ou Combat)
		
	}
	
	
}


