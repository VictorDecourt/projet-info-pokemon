    # Pokemon

Cette application permet de jouer à un jeu inspiré de la série de jeux vidéo Pokemon. 

 ## <a name="sommaire"></a>Sommaire

 * [Installation](#installation)
 * [Comment jouer ?](#tuto)
 * [développeurs](#développeurs)


## <a name="installation"></a> Installation
Afin de pouvoir utiliser notre application, télechargez tous les fichiers du gitHUB en cliquant sur [ce lien](https://github.com/blabla.git)
Ensuite lancez [tel fichier]() 
Vous pouvez ensuite choisir les différents paramètres et observer votre simulation.


## <a name="tuto"></a> Comment jouer ?
Une fois l'application lancée, vous vous retrouvez dans le monde des pokemons. Déplacez vous alors avec les flèches directionnelles.
Au cours de vos pérégrinations vous vous ferez attaquer par des pokemons sauvages. Lors d'un combat contre un pokemon sauvage vous pouvez l'attaquer, 
utiliser des objets de soin, changer de pokemon et tenter de le capturer.
Après un combat vous pouvez vous rendre dans un centre de soin pour soigner votre équipe, pour la soigner rendez vous sur la case du soigneur.


## <a name="developpeurs"></a>Développeurs
* Hugo TARDY :  <hugo.tardy@ensg.eu>
* Julien BARNEOUD :  <julien.barneoud@ensg.eu>
* Victor DECOURT :  <victor.decourt@ensg.eu>

</br>Projet réalisé dans le cadre de l'enseignement de première année d'école d'ingénieur à l'ENSG-Marne-la-Vallee.
 
